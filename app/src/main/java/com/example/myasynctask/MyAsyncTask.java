package com.example.myasynctask;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.view.View;
import java.util.Random;
import okhttp3.Call;

class MyAsyncTask extends AsyncTask<String,Integer,String>{
    @Override
    protected void onPreExecute() {
        MainActivity.progressBar.setVisibility(View.VISIBLE);
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(String... strings) {
        int step = 1;
        try {
            while (step <= 100) {
                Random r = new Random();
                publishProgress(step+=r.nextInt(10));
                Thread.sleep(50);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        OkhttpUtil.okHttpGetBitmap("https://wx4.sinaimg.cn/mw690/c9c053f8gy1h0e2a9nlcuj20io0p0q5k.jpg", new CallBackUtil.CallBackBitmap() {
            @Override
            public void onFailure(Call call, Exception e) {
            }

            @Override
            public void onResponse(Bitmap response) {
                MainActivity.imageView.setImageBitmap(response);
            }
        });

        return "下载完成";
    }

    @Override
    protected void onPostExecute(String s) {
        MainActivity.button.setText(s);
        super.onPostExecute(s);
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        MainActivity.progressBar.setProgress(values[0]); //更新ProgressBar
        super.onProgressUpdate(values);
        MainActivity.button.setText("后台正在下载...");
    }
}
